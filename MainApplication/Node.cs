﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MainApplication
{
    public class Node
    {
        private Node left;
        private Node right;
        private int data;
        private int count;

        public Node(int data)
        {
            this.data = data;
            this.left = null;
            this.right = null;
            this.count = 1;
        }

        internal void Add(int newData)
        {
            // Är det nya värdet som vi vill lägga till
            // mindre än det aktuella?
            if (newData < data)
            {
                // Ja, kontrollera om vi är längst ned i trädstrukturen.
                if (left == null)
                {
                    // Ja, det är vi. Lägg till det nya värdet i den vänstra noden
                    // då värdet är mindre än det aktuella.
                    left = new Node(newData);
                }
                else
                {
                    // Nej, traversera vidare ned i trädstrukturen.
                    left.Add(newData);
                }
            }
            else if (newData > data)
            {
                // Nej, det nya värdet är större än det aktuella.
                // Kontrollera om vi är längst ned i trädstrukturen.
                if (right == null)
                {
                    // Ja, det är vi. Lägg till det nya värdet i den högra noden
                    // då värdet är större än det aktuella.
                    right = new Node(newData);
                }
                else
                {
                    // Nej, traverser vidare ned i trädstrukturen.
                    right.Add(newData);
                }
            }
            else
            {
                count++;
            }
        }

        /// <summary>
        /// Metod för att traversera trädet Inorder.
        /// </summary>
        internal void Inorder()
        {
            if (left != null)
            {
                left.Inorder();
            }

            Console.Write(data + ",");

            if (right != null)
            {
                right.Inorder();
            }
        }

        internal void Preorder(List<int> nodeValues)
        {
            nodeValues.Add(this.Data);

            if (left != null)
            {
                left.Preorder(nodeValues);
            }

            if (right != null)
            {
                right.Preorder(nodeValues);
            }
        }

        internal void Postorder()
        {
            if (left != null)
            {
                left.Postorder();
            }

            if (right != null)
            {
                right.Postorder();
            }

            Console.Write(data + ",");
        }

        /// <summary>
        /// Beräknar den största nivån i ett träd.
        /// </summary>
        /// <returns></returns>
        internal int Level()
        {
            int leftLevel = 0;
            int rightLevel = 0;

            if (left != null)
            {
                leftLevel = left.Level();
            }

            if (right != null)
            {
                rightLevel = right.Level();
            }

            if (leftLevel > rightLevel)
            {
                return leftLevel + 1;
            }
            
            return rightLevel + 1;
        }

        internal int Leaf()
        {
            int leafs = 0;

            // Finns det några noder till vänster?
            if (left != null)
            {
                // Ja, fortsätt traversera ned i strukturen.
                // Addera resultatet till det totala antalet löv.
                leafs += left.Leaf();
            }

            // Finns det några noder till höger?
            if (right != null)
            {
                // Ja, fortsätt traversera ned i strukturen.
                // Addera resultatet till det totala anatet löv.
                leafs += right.Leaf();
            }

            // Om vi är i ett löv så ska både vänster- och höger-noden
            // vara null. Om så är fallet så ska denna räknas som ett löv.
            if (left == null && right == null)
            {
                leafs++;
            }

            // Returnera antalet löv.
            return leafs;
        }

        /// <summary>
        /// Kontrollerar om ett träd är balanserat.
        /// </summary>
        /// <returns>
        /// True om trädet är balanserat, annars False.
        /// </returns>
        internal bool Balance()
        {
            // Beräkna nodnivåerna för den högra och den vänstra sidan.
            int leftLevel = left != null ? left.Level() : 0;
            int rightLevel = right != null ? right.Level() : 0;

            // Initierar kontrollflaggorna.
            bool leftBalance = true;
            bool rightBalance = true;

            // Om skillnaden i nodnivåer är mer än 1 så är trädet i obalans.
            // Avsluta kontrollen genom att returnera False.
            if (Math.Abs(leftLevel - rightLevel) > 1)
            {
                return false;
            }

            // Om vänsternoden inte är null så ska vi fortsätta genomföra kontrollen.
            if (left != null)
            {
                leftBalance = left.Balance();
            }

            // Om högernoden inte är null så ska vi fortsätta genomföra kontrollen.
            if (right != null)
            {
                rightBalance = right.Balance();
            }

            // Kontrollera om vänster och högersidan är balanserade.
            // I så fall returnerar vi True, annars False.
            return (leftBalance == rightBalance);
        }

        /// <summary>
        /// Letar upp en nod med angivet sökvärde.
        /// </summary>
        /// <param name="value">Värde som eftersöks.</param>
        /// <returns>
        /// Antal förekomster av det eftersökta värdet. Finns inte värdet
        /// i trädet returneras 0.
        /// </returns>
        internal int Find(int value)
        {
            int found = 0;

            // Innehåller denna nod det eftersöka värdet?
            // I så fall returnerar vi det.
            if (data == value)
                return count;

            // Traversera vidare till vänster om vi inte är i botten av trädet.
            if (left != null)
            {
                found = left.Find(value);
            }

            // Traversera vidare till höger om vi inte är i botten av trädet eller
            // om det eftersökta värdet inte fanns på den vänstra sidan.
            if (right != null && found == 0)
            {
                return right.Find(value);
            }

            // Returnera antalet av det eftersökta värdet.
            return found;
        }

        /// <summary>
        /// Summerar det totala värdet som lagras i samtliga noder.
        /// </summary>
        internal int Sum()
        {
            int sum = 0;

            // Är vi inte i slutet av trädet så ska vi fortsätta traversera till vänster.
            if (left != null)
            {
                sum = left.Sum();    
            }

            // Är vi inte i slutet av trädet så ska vi fortsätta traversera till höger.
            if (right != null)
            {
                sum += right.Sum();
            }

            // Returnera den nuvarande totalsumman plus det värde som är lagrat i denna nod multiplicerat
            // med alla förekomster
            return (sum + (data * count));
        }

        internal void Remove(int value, Node parent, Node current)
        {
            //  Om current är null, avbryt
            if (current == null)
                return;

            // Om det inte är det sökta värdet i current
            // och det sökta värdet är mindre, fortsätt leta till vänster.
            if (current.Data != value && value < current.Data)
            {
                Remove(value, current, current.Left);
            }
            else if (current.Data != value && value > current.Data)
            {
                Remove(value, current, current.Right);
            }
            else
            {
                // Det eftersökta värdet finns i denna nod!
                // Om current inte har något till höger så ska 
                // förälderns vänsterreferens sättas till samma
                // värde som currents vänsterreferens.
                if (current.Left == null)
                {
                    if (parent.Left == current)
                    {
                        parent.Left = current.Right;
                    }
                    else
                    {
                        parent.Right = current.Right;
                    }
                }
                else if (current.Right == null)
                {
                    if (parent.Left == current)
                    {
                        parent.Left = current.Left;
                    }
                    else
                    {
                        parent.Right = current.Left;
                    }
                }

                // Om det finns två barn
                if (current.Left != null && current.Right != null)
                {
                    // Kontrollera att det finns ett värde current.Left.Right
                    if (current.Left.Right != null)
                    {
                        // Hämta utbytningsvärde från GetAt
                        current.Data = GetAt(current, current.Left);
                    }
                    else
                    {
                        // Lägg in det värde som finns till vänster 
                        current.Data = current.Left.Data;
                        current.Left = current.Left.Left;
                    }
                }
            }
        }

        public int GetAt(Node parent, Node current)
        {
            // Om det finns barn till höger, fortsätt neråt höger
            if (current.Right != null)
            {
                return GetAt(current, current.Right);
            }

            parent.Right = current.Left;
            
            return current.Data;
        }

        /// <summary>
        /// Hämtar värdet i noden.
        /// </summary>
        internal int Data 
        { 
            get
            {
                return data;
            }

            set
            {
                data = value;
            }
        }


        /// <summary>
        /// Hämtar referensen till vänsterpekaren.
        /// </summary>
        internal Node Left
        {
            get
            {
                return left;
            }

            set
            {
                left = value;
            }
        }

        /// <summary>
        /// Hämtar referensen till högerpekaren.
        /// </summary>
        internal Node Right
        {
            get
            {
                return right;
            }

            set
            {
                right = value;
            }
        }


    }
}
