﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace MainApplication
{
    public class Circle : Shape
    {
        public int Radius { get; set; }

        public override void Paint(Graphics g)
        {
            SizeF textSize = TextSize(g);
            g.FillEllipse(new SolidBrush(Color.Black), this.X, this.Y, this.Radius * 2, this.Radius * 2);
            g.DrawString(this.Text, new Font("Arial", 8), new SolidBrush(Color.Red), this.X + this.Radius - (textSize.Width / 2), this.Y + this.Radius - (textSize.Height / 2));
        }
    }
}
