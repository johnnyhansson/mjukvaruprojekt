﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace MainApplication
{
    public class Rectangle : Shape
    {
        public int Width { get; set; }
        public int Height { get; set; }

        public override void Paint(Graphics g)
        {
            SizeF textSize = TextSize(g);
            g.FillRectangle(new SolidBrush(Color.Black), this.X, this.Y, this.Width, this.Height);
            g.DrawString(this.Text, new Font("Arial", 8), new SolidBrush(Color.Red), this.X + (this.Width / 2) - (textSize.Width / 2), this.Y + (this.Height / 2) - (textSize.Height / 2));
        }
    }
}
