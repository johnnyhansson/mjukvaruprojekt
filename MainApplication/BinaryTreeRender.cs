﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace MainApplication
{
    class BinaryTreeRender
    {
        const int RADIUS = 10;
        const int DISTANCEX = 30;
        const int DISTANCEY = 50;

        public void Paint(Graphics g, int width, int height, Tree tree)
        {
            CalculateDistance(g, width / 4, DISTANCEY, tree.Root, tree.Root);
        }

        private void CalculateDistance(Graphics g, float x, float y, Node root, Node child)
        {
            if (child.Data <= root.Data)
            {
                Paint(g, x, y, x - 20, child);
                
                if (child.Left != null)
                {
                    CalculateDistance(g, x - DISTANCEX, y + DISTANCEY, root, child.Left);
                    //Paint(g, x - DISTANCEX, y + DISTANCEY, node.Left, level + 1);
                    //g.DrawLine(new Pen(Color.Blue), x - DISTANCEX + (RADIUS * 2), y + DISTANCEY, x, y + (RADIUS * 2));
                }

                if (child.Right != null)
                {
                    CalculateDistance(g, x + DISTANCEX, y + DISTANCEY, root, child.Right);
                    //Paint(g, x + DISTANCEX, y + DISTANCEY, node.Right, level + 1);
                    //g.DrawLine(new Pen(Color.Blue), x + (RADIUS * 2), y + (RADIUS * 2), x + DISTANCEX, y + DISTANCEY);
                }
            }
            else
            {
                Paint(g, x, y, x + 20, child);

                if (child.Left != null)
                {
                    CalculateDistance(g, x - DISTANCEX, y + DISTANCEY, root, child.Left);
                    //Paint(g, x - DISTANCEX, y + DISTANCEY, node.Left, level + 1);
                    //g.DrawLine(new Pen(Color.Blue), x - DISTANCEX + (RADIUS * 2), y + DISTANCEY, x, y + (RADIUS * 2));
                }

                if (child.Right != null)
                {
                    CalculateDistance(g, x + DISTANCEX, y + DISTANCEY, root, child.Right);
                    //Paint(g, x + DISTANCEX, y + DISTANCEY, node.Right, level + 1);
                    //g.DrawLine(new Pen(Color.Blue), x + (RADIUS * 2), y + (RADIUS * 2), x + DISTANCEX, y + DISTANCEY);
                }
            }
        }

        private void Paint(Graphics g, float x, float y, float distance, Node node)
        {
            Shape shape = new Circle() { X = (x + distance), Y = y, Text = node.Data.ToString(), Radius = RADIUS };
            shape.Paint(g);
        }
    }
}
