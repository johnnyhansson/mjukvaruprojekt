﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MainApplication
{
    public class Tree
    {
        private Node root;

        public Node Root
        {
            get { return root; }
        }

        public Tree()
        {
            root = null;
        }

        public void Add(int data)
        {
            if (root == null)
            {
                root = new Node(data);
            }
            else
            {
                root.Add(data);
            }
        }

        public void Inorder()
        {
            if (root != null)
            {
                root.Inorder();
            }
        }

        public List<int> Preorder()
        {
            List<int> nodeValues = new List<int>();

            if (root != null)
            {
                root.Preorder(nodeValues);
            }

            return nodeValues;
        }

        public void Postorder()
        {
            if (root != null)
            {
                root.Postorder();
            }
        }

        public int Level()
        {
            if (root != null)
            {
                return root.Level();
            }

            return 0;
        }

        public int Leaf()
        {
            if (root != null)
            {
                return root.Leaf();
            }

            return 0;
        }

        public bool IsBalanced()
        {
            if (root != null)
            {
                return root.Balance();
            }

            return true;
        }

        public int Find(int value)
        {
            if (root != null)
            {
                return root.Find(value);
            }

            return 0;
        }

        public int Sum()
        {
            if (root != null)
            {
                return root.Sum();
            }

            return 0;
        }

        public void Remove(int value)
        {
            // Om ingen nog finns så gör vi ingenting.
            if (root != null)
            {
                // Innehåller rotnoden det värdet som ska raderas?
                if (root.Data != value)
                {
                    // Nej, fortsätt leta.
                    root.Remove(value, root, root);
                }
                else
                {
                    // Ja, kontrollera om rotnoden har en vänsterpekare.
                    if (root.Left != null)
                    {
                        // Länka om roten till den nuvarande rotens vänsterpekare.
                        root = root.Left;
                    }
                    else // Roten ska bort
                    {
                        if (root.Left == null)
                        {
                            root = root.Right;
                        }
                        else if (root.Right == null)
                        {
                            root = root.Left;
                        }
                    }

                    if (root.Left.Right != null)
                    {
                        // Om noden bara har två barn
                        // Anropa GetAt (root, root.Left)
                        // Byt till värdet från GetAt
                        root.Data = root.GetAt(root, root.Left);
                    }
                    else
                    {
                        root.Data = root.Left.Data;
                        root.Left = root.Left.Left;
                    }
                }
            }
        }
    }
}
