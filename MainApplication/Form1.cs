﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MainApplication
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void OnPaint(object sender, PaintEventArgs e)
        {
            BinaryTreeRender render = new BinaryTreeRender();
            Tree tree = new Tree();
            tree.Add(6);
            tree.Add(4);
            tree.Add(3);
            tree.Add(2);
            tree.Add(1);
            tree.Add(55);
            tree.Add(44);
            tree.Add(56);
            tree.Add(5);
            tree.Add(40);

            render.Paint(e.Graphics, this.Width, this.Height, tree);
            //Circle c = new Circle() { X = 50, Y = 50, Radius = 20, Text = "Tjoff" };
            //c.Paint(e.Graphics);
        }
    }
}
