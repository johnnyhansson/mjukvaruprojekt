﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace MainApplication
{
    public abstract class Shape
    {
        public abstract void Paint(Graphics g);

        protected SizeF TextSize(Graphics g)
        {
            return g.MeasureString(this.Text, new Font("Arial", 8));
        }

        public float X { get; set; }
        public float Y { get; set; }
        public string Text { get; set; }
    }
}
